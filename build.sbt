name := "matching"

version := "0.1"

scalaVersion := "2.12.6"


libraryDependencies += "io.monix" %% "monix" % "3.0.0-RC1"
libraryDependencies += "org.typelevel" %% "cats-core" % "1.2.0"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"