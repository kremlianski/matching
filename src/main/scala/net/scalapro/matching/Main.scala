package net.scalapro.matching

import net.scalapro.matching.model._
import monix.reactive.Observable
import scala.io.Source
import exchange._

object Main extends App {
  val clientsFilename = "files/clients.txt"
  val ordersFilename = "files/orders.txt"
  lazy val clients: List[Client] = Source.fromFile(clientsFilename).getLines.toList.map { line =>
    line.split("\t") match {
      case Array(id, cash, a, b, c, d) => Client(id, cash.toInt, Map(
        "A" -> a.toInt,
        "B" -> b.toInt,
        "C" -> c.toInt,
        "D" -> d.toInt
      ))
    }
  }


  lazy val initialState = ExchangeState(clients.map(x => x.id -> x).toMap)

  val ordersStream: Observable[Order] = Observable.fromIterator(Source.fromFile(ordersFilename).getLines).map { l =>

    l.split("\t") match {
      case Array(clientId, t, stock, price, amount) if t == "s" => Sell(clientId, stock, price.toInt, amount.toInt)
      case Array(clientId, t, stock, price, amount) if t == "b" => Buy(clientId, stock, price.toInt, amount.toInt)
    }
  }

  val text = matchOrders(initialState, ordersStream).fold(""){state: ExchangeState =>

    state.clients.values.toList.foldLeft(""){(a, c) => a + c.toLine}

  }

  import java.io._
  val pw = new PrintWriter(new File("files/result.txt" ))
  pw.write(text)
  pw.close


}
