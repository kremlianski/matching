package net.scalapro.matching.model

case class Client(
                   id: ClientId,
                   cash: Int = 0,
                   assets: Map[StockId, Int] = Map.empty
                 ) {
  def toLine(): String = s"$id\t$cash\t${assets.getOrElse("A", 0)}\t${assets.getOrElse("B", 0)}\t${assets.getOrElse("C", 0)}\t${assets.getOrElse("D", 0)}\n"
}

