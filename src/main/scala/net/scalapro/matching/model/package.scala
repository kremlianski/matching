package net.scalapro.matching

package object model {
  type ClientId = String
  type StockId = String
}
