package net.scalapro.matching.model

import java.util.UUID


sealed trait Order {
  def clientId: ClientId

  def stock: StockId

  def price: Int

  def amount: Int

  def id: UUID
}

case class Sell(
                clientId: ClientId,
                stock: StockId,
                price: Int,
                amount: Int,
                id: UUID = UUID.randomUUID()
              ) extends Order

case class Buy (
                clientId: ClientId,
                stock: StockId,
                price: Int,
                amount: Int,
                id: UUID = UUID.randomUUID()
              ) extends Order

case class ExchangeState(
                          clients: Map[ClientId, Client] = Map.empty,
                          orders: Map[StockId, List[Order]] = Map.empty
                        )


