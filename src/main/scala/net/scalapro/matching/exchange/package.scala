package net.scalapro.matching

import monix.reactive.Observable
import net.scalapro.matching.model.{Buy, ExchangeState, Order, Sell}
import scala.concurrent.Await
import scala.concurrent.duration._
import monix.execution.Scheduler.Implicits.global
import cats.{Order => _, _}
import cats.implicits._

package object exchange {

  private def migrate(state: ExchangeState, order: Order, winner: Order): ExchangeState = {

    val ordersStock = state.orders.getOrElse(winner.stock, List.empty).filterNot(_.id == winner.id)

    val (sell, buy) = (order, winner) match {
      case (_: Sell, _: Buy) => (order, winner)
      case (_: Buy, _: Sell) => (winner, order)
    }

    val clientSell = state.clients
      .getOrElse(sell.clientId, throw new Exception(s"no client with id ${sell.clientId}"))

    val clientSellAssets = clientSell.assets ++ Map(sell.stock -> (clientSell.assets.getOrElse(sell.stock, 0) - sell.amount))
    val clientSellCash = clientSell.cash + (sell.amount * sell.price)

    val clientBuy = state.clients
      .getOrElse(buy.clientId, throw new Exception(s"no client with id ${buy.clientId}"))

    val clientBuyAssets = clientBuy.assets |+| Map(buy.stock -> buy.amount)
    val clientBuyCash = clientBuy.cash - (sell.amount * sell.price)


    state.copy(
      orders = state.orders ++ Map(winner.stock -> ordersStock),
      clients = state.clients ++ Map(clientBuy.id -> clientBuy.copy(
        assets = clientBuyAssets, cash = clientBuyCash
      )) ++ Map(clientSell.id -> clientSell.copy(
        assets = clientSellAssets, cash = clientSellCash
      ))
    )
  }

  def matchOrders(initialState: => ExchangeState, ordersStream: Observable[Order]): Option[ExchangeState] = {
    val exchange = ordersStream.scan(initialState){(s, o) =>
      o match {
        case ord @ Sell(clientId, stock, price, amount, _) =>
          val matchList = s.orders.getOrElse(stock, List.empty).collect{
            case b @ Buy(cId, _, pr, am, _) if cId != clientId && pr == price && am == amount => b
          }
          if(matchList.nonEmpty) {
            val winner: Order = matchList.head
            migrate(s, ord, winner)
          } else s.copy(orders = s.orders |+| Map(stock ->  List(ord)) )
        case ord @ Buy(clientId, stock, price, amount, _) =>
          val matchList = s.orders.getOrElse(stock, List.empty).collect{
            case b @ Sell(cId, _, pr, am, _) if cId != clientId && pr == price && am == amount => b
          }
          if(matchList.nonEmpty) {
            val winner: Order = matchList.head
            migrate(s, ord, winner)
          } else s.copy(orders = s.orders |+| Map(stock ->  List(ord)) )
      }
    }

    val f = exchange.runAsyncGetLast
    val res: Option[ExchangeState] = Await.result(f, 10 seconds)
    res
  }

}
