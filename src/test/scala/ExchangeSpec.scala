import java.util.UUID

import monix.reactive.Observable
import org.scalatest.{FlatSpec, Matchers}
import net.scalapro.matching.exchange._
import net.scalapro.matching.model._

class ExchangeSpec extends FlatSpec with Matchers {

  "matchOrders method" must "produce nothing if there are no Orders" in {

    val stream = Observable.fromIterator(List.empty[Order].toIterator)

    matchOrders(ExchangeState(clients), stream) shouldBe None

  }

  it must "store the first order in the state" in {

    val id = UUID.randomUUID()
    val list = List(Sell("C8", "C", 15, 4, id))
    val stream = Observable.fromIterator(list.toIterator)

    val clients = Map("C8" -> Client("C8"))


    val state = matchOrders(ExchangeState(clients), stream)
    state.flatMap(_.orders.get("C")) shouldBe Some(list)
    state.map(_.clients) shouldBe Some(clients)

  }

  it must "match two Orders (sell & buy) with same stock, price, amount" in {

    val clients = Map("C8" -> Client("C8"), "C7" -> Client("C7"))

    val list = List(Sell("C8", "C", 15, 4), Buy("C7", "C", 15, 4))
    val stream = Observable.fromIterator(list.toIterator)

    val state = matchOrders(ExchangeState(clients), stream)
    state.flatMap(_.orders.get("C")) shouldBe Some(List.empty)
    state.map(_.clients) shouldBe Some(Map("C8" -> Client(
      "C8", 60, Map("C" -> -4)), "C7" -> Client("C7", -60, Map("C" -> 4)))
    )
  }

  it must "take first of the orders in the queue with same stock, price, amount" in {

    val id1 = UUID.randomUUID()
    val id2 = UUID.randomUUID()
    val id = UUID.randomUUID()

    val clients = Map("C8" -> Client("C8"), "C7" -> Client("C7"))

    val list = List(Sell("C8", "C", 15, 4, id1), Sell("C8", "C", 15, 4, id2), Buy("C7", "C", 15, 4, id))
    val stream = Observable.fromIterator(list.toIterator)

    val state = matchOrders(ExchangeState(clients), stream)
    state.flatMap(_.orders.get("C")) shouldBe Some(List(Sell("C8", "C", 15, 4, id2)))
    state.map(_.clients) shouldBe Some(Map("C8" -> Client(
      "C8", 60, Map("C" -> -4)), "C7" -> Client("C7", -60, Map("C" -> 4)))
    )

  }

  it must "don't match two Orders (sell & buy) with same stock, price, amount but from one client" in {


    val clients = Map("C8" -> Client("C8"))

    val list = List(Sell("C8", "C", 15, 4), Buy("C8", "C", 15, 4))
    val stream = Observable.fromIterator(list.toIterator)

    val state = matchOrders(ExchangeState(clients), stream)
    state.flatMap(_.orders.get("C").map(_.size)) shouldBe Some(2)

    state.map(_.clients) shouldBe Some(clients)

  }

  it must "don't match two Orders (sell & buy) with same stock, price but different amount" in {

    val clients = Map("C8" -> Client("C8"), "C6" -> Client("C6"))

    val list = List(Sell("C8", "C", 15, 4), Buy("C6", "C", 15, 3))
    val stream = Observable.fromIterator(list.toIterator)

    matchOrders(ExchangeState(clients), stream).flatMap(_.orders.get("C").map(_.size)) shouldBe Some(2)

  }

  it must "don't match two Orders (sell & buy) with same stock, amount but different price" in {

    val clients = Map("C8" -> Client("C8"), "C6" -> Client("C6"))

    val list = List(Sell("C8", "C", 15, 4), Buy("C6", "C", 16, 4))
    val stream = Observable.fromIterator(list.toIterator)

    matchOrders(ExchangeState(clients), stream).flatMap(_.orders.get("C").map(_.size)) shouldBe Some(2)

  }

  val clients = Map("C8" -> Client("C8"), "C7" -> Client("C7"))

  it must "match two Orders (buy & sell) with same stock, price, amount" in {

    val list = List(Buy("C7", "C", 15, 4), Sell("C8", "C", 15, 4))
    val stream = Observable.fromIterator(list.toIterator)

    val state = matchOrders(ExchangeState(clients), stream)
    state.flatMap(_.orders.get("C")) shouldBe Some(List.empty)
    state.map(_.clients) shouldBe Some(Map("C8" -> Client(
      "C8", 60, Map("C" -> -4)), "C7" -> Client("C7", -60, Map("C" -> 4)))
    )

  }

  it must "don't match two Orders (buy & sell) with price, amount but different stock" in {

    val id1 = UUID.randomUUID()
    val id2 = UUID.randomUUID()

    val list = List(Buy("C7", "C", 15, 4, id1), Sell("C8", "D", 15, 4, id2))
    val stream = Observable.fromIterator(list.toIterator)

    val clients = Map("C8" -> Client("C8"), "C7" -> Client("C7"))

    matchOrders(ExchangeState(clients), stream).map(_.orders) shouldBe Some(
      Map("D" -> List(Sell("C8", "D", 15, 4, id2)), "C" -> List(Buy("C7", "C", 15, 4, id1)))
    )
  }
}
